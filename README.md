# [Cryptofex: Bring Your Own App](https://cryptofex.io/byoa/)



## Submission

1. Create a fork of this repository.
2. Copy the `submissions/submission_template.md` file to `submissions/{PROJECT NAME}.md` and fill out the information.
3. You may add picture next to your submission `submissions/{PROJECT NAME}.jpg` or use a URL.
3. Create a merge request onto [this repo](https://gitlab.com/pyrofex/cryptofex--bring-your-own-app) with your submission.
4. We will approve your submission soon.
5. You can continue to make changes to your project after submission. We will look at the git history up until the contest deadline.


## Official Rules

[See Here](RULES.md)
